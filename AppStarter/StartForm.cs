﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace AppStarter
{
    public partial class StartForm : Form
    {
        private Process strtMainProg;
        private Logger logger;
        private bool MainProgWasStarted = false;

        public StartForm()
        {
            InitializeComponent();
            logger = new Logger();
            strtMainProg = new Process();
            strtMainProg.StartInfo.FileName = Properties.Settings.Default.MainProgramPath;
            
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (tbxReg.Text != "" && tbxReg.Text != "Введите ФИО")
            {
                try
                {
                    strtMainProg.Start();
                    strtMainProg.EnableRaisingEvents = true;
                    MainProgWasStarted = true;
                    strtMainProg.Exited += new EventHandler(MainProg_Exited);

                }
                catch (Exception)
                {
                    MessageBox.Show("Проблемы с запуском основной программы!\nПрограммы " + Properties.Settings.Default.MainProgramPath + " не существует! \nПроверьте файл настроек", "Данное приложение будет закрыто", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Application.Exit();
                }
                btnStart.Enabled = false;
                this.Hide();
            }
            else
            {
                MessageBox.Show("Поле ФИО пустое!", "Введите данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbxReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
                this.tbxReg.Text = "Введите ФИО";
                this.btnStart.Focus();
            }
        }

        private void MainProg_Exited(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void StartForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(MainProgWasStarted) logger.WriteLine(DateTime.Now.ToShortDateString() + ", host: " + Environment.MachineName + ", Full Name: " + tbxReg.Text
                + ", t_start: " + strtMainProg.StartTime.ToLongTimeString() + ", t_stop: " + strtMainProg.ExitTime.ToLongTimeString());
        }

        private void tbxReg_Enter(object sender, EventArgs e)
        {
            this.tbxReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbxReg.Clear();
        }

        private void StartForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnStart.PerformClick();
            }
        }
    }
}

﻿using System;
using System.IO;
using System.Windows.Forms;

namespace AppStarter
{
    /// <summary>
    /// Logger class
    /// </summary>
    class Logger
    {
        private string LogFilePath;

        public Logger()
        {
            LogFilePath = ".\\AppStarter.log";
        }
        /// <summary>
        /// Writes a new line to LogFile and adds a character '\n' at the end
        /// </summary>
        public void WriteLine(string logText)
        {
            try
            {
                using (StreamWriter logFile = new StreamWriter(LogFilePath, true))
                {
                    logFile.WriteLine(logText);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Запись данных в лог не была произведена");
            }
        }
        /// <summary>
        /// Append a new string to LogFile without character '\n' at the end
        /// </summary>
        public void WriteString(string logText)
        {
            try
            {
                using (StreamWriter logFile = new StreamWriter(LogFilePath, true))
                {
                    logFile.Write(logText);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Запись данных в лог не была произведена");
            }
        }
    }
}
